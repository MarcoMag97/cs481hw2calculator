﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace CS433HW2Calculator
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string button_press = button.Text;

            Value.Text += button_press;

        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            Value.Text = "";
        }
    }
}
